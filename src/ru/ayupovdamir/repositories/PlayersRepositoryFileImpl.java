package ru.ayupovdamir.repositories;

import ru.ayupovdamir.models.Player;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class PlayersRepositoryFileImpl implements PlayersRepository {
    private final String FILE_NAME = "Players.txt";

    @Override
    public void save(Player player) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(FILE_NAME));

            writer.close();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public boolean containsNickname(String nickname) {
        return false;
    }

    @Override
    public Player findPlayerByNickname(String firstPlayer) {
        return null;
    }
}
