package ru.ayupovdamir.repositories;

import ru.ayupovdamir.models.Game;
import ru.ayupovdamir.models.Player;

import java.io.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GamesRepositoryFileImpl implements GamesRepository {

    private String fileName;

    public GamesRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    private Mapper<Game, String> gameToStringLineMapper = game -> {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(game.getId())
                .append("|").append(game.getStartGameDateTime())
                .append("|");
        if(game.getFinishDateTime() == null){
            stringBuilder.append("Null").append("|");
        }
        else {
            stringBuilder.append(game.getFinishDateTime()).append("|");
        }
        stringBuilder.append(game.getFirstPlayer().getNickname()).append("|")
                .append(game.getSecondPlayer().getNickname()).append("|");
        if(game.getShotsCount() == null){
            stringBuilder.append("Null");
        }
        else {
            stringBuilder.append(game.getShotsCount());
        }
        return stringBuilder.toString();
    };

    private Mapper<String, Game> stringToGameMapper = line -> {
        String[] gameInString = line.split("\\|");
        Game result = new Game(gameInString[0]);
        result.setStartGameDateTime(LocalDateTime.parse(gameInString[1]));
        if(gameInString[2].equals("Null")){
            result.setFinishDateTime(null);
        }
        else {
            result.setFinishDateTime(LocalDateTime.parse(gameInString[2]));
        }
        result.setFirstPlayer(new Player(gameInString[3]));
        result.setSecondPlayer(new Player(gameInString[4]));
        if(gameInString[5].equals("Null")){
            result.setShotsCount(null);
        }
        else {
            result.setShotsCount(Integer.parseInt(gameInString[5]));
        }
        return result;
    };

    @Override
    public void save(Game game) {
        String lineToWrite = gameToStringLineMapper.map(game);
        try{
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName, true));
            bufferedWriter.write(lineToWrite + "\n");
           bufferedWriter.close();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public Game findGameById(String gameId) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String lineToFile = bufferedReader.readLine();
            while (lineToFile != null){
                Game game = stringToGameMapper.map(lineToFile);
                if (game.getId().equals(gameId)){
                    return game;
                }
                lineToFile = bufferedReader.readLine();
            }
            return null;

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Game game) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));

            List<Game> gamesFromFile = reader.lines() //получаем все строки из файла
                    .map(stringToGameMapper::map).collect(Collectors.toList());

            Game gameFromFlie = gamesFromFile.stream() //преобразуем строку в Game
                    .filter(existedGame -> existedGame.getId().equals(game.getId())) //находим все игры с нужным элементом
                    .limit(1) //ограничиваем выборку до одного элемента
                    .collect(Collectors.toList()) //получаем в виде списка
                    .get(0); //вытаскиваем первый элемент

            gameFromFlie.setFinishDateTime(game.getFinishDateTime());
            gameFromFlie.setShotsCount(game.getShotsCount());

            gamesFromFile = gamesFromFile.stream().map(existedGame ->
            {
                if (existedGame.getId().equals(gameFromFlie.getId())){
                    return gameFromFlie;
                }
                else {
                    return existedGame;
                }
            }).collect(Collectors.toList());

            gamesFromFile.removeIf(existedGame -> existedGame.getId().equals(gameFromFlie.getId()));
            gamesFromFile.add(gameFromFlie);
            reader.close();

            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
            gamesFromFile.forEach(newGame -> {
                try {
                    writer.write(gameToStringLineMapper.map(newGame) + "\n");
                } catch (IOException e) {
                    throw new IllegalArgumentException(e);               }
            });
            writer.close();
        } catch (IOException e){
            throw new IllegalArgumentException(e);
        }
    }
}
