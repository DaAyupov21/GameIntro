package ru.ayupovdamir.repositories;

import jdk.jshell.spi.ExecutionControl;
import ru.ayupovdamir.models.Game;

import java.util.*;


public class GamesRepositoryImpl implements GamesRepository {
    private List<Game> games;

    public GamesRepositoryImpl() {
        this.games = new ArrayList<>();
    }

    @Override
    public void save(Game game) {
        games.add(game);
    }

    @Override
    public Game findGameById(String gameId) {
        for(Game game : games){
            if(game.getId().equals(gameId)){
                return game;
            }
        }
        return null;
    }

    @Override
    public void update(Game game) {
        int i = 5;
    }
}
