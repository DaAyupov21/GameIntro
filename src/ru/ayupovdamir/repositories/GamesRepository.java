package ru.ayupovdamir.repositories;

import jdk.jshell.spi.ExecutionControl;
import ru.ayupovdamir.models.Game;

public interface GamesRepository {
    /**
     * Сохранение игры
     * @param game игру которую нужно сохранить
     */
    void save(Game game);

    Game findGameById(String gameId);

    void update (Game game) throws ExecutionControl.NotImplementedException;
}
