package ru.ayupovdamir.repositories;

import ru.ayupovdamir.models.Player;

public interface PlayersRepository {
    /**
     * Сохраняет игрока в памяти
     * @param player игрок которого нужно сохранить
     */
    void save(Player player);


    /**
     * Смотрит есть ли игрок в системе с таким именем
     * @param nickname имя которое нужно проверить
     * @return true если игрок с таким именем уже играл
     */
    boolean containsNickname(String nickname);

    /**
     * Получить игрока по его никнейму
     * @param firstPlayer никнейм, который мы ищем
     * @return игрок с нужным нам никнеймом
     */
    Player findPlayerByNickname(String firstPlayer);
}
