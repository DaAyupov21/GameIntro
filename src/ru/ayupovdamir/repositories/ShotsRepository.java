package ru.ayupovdamir.repositories;

import ru.ayupovdamir.models.Shot;

public interface ShotsRepository {
    void save(Shot shot);

}
