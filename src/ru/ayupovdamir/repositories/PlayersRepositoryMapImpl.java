package ru.ayupovdamir.repositories;

import ru.ayupovdamir.models.Player;

import java.util.HashMap;
import java.util.Map;

public class PlayersRepositoryMapImpl implements PlayersRepository {

    private Map<String, Player> players;

    public PlayersRepositoryMapImpl() {
        this.players = new HashMap<>();
    }

    @Override
    public void save(Player player) {
        this.players.put(player.getNickname(), player);
    }

    @Override
    public boolean containsNickname(String nickname) {
        return this.players.containsKey(nickname);
    }

    @Override
    public Player findPlayerByNickname(String firstPlayer) {
        return this.players.get(firstPlayer);
    }
}
