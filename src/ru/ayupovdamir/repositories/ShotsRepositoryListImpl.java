package ru.ayupovdamir.repositories;

import ru.ayupovdamir.models.Shot;

import java.util.*;

public class ShotsRepositoryListImpl implements ShotsRepository {
    private List<Shot> shots;

    public ShotsRepositoryListImpl() {
        shots = new ArrayList<>();
    }

    @Override
    public void save(Shot shot) {
        shots.add(shot);
    }
}
