package ru.ayupovdamir.repositories;

public interface Mapper<T, V>{
    V map(T object);
}
