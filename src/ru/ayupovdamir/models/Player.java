package ru.ayupovdamir.models;

public class Player {
    private String nickname;
    private Integer score; //сколько раз он попал в противника
    private Integer winsCount;
    private Integer losesCount;

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public void setWinsCount(Integer winsCount) {
        this.winsCount = winsCount;
    }

    public void setLosesCount(Integer losesCount) {
        this.losesCount = losesCount;
    }

    public String getNickname() {
        return nickname;
    }

    public Integer getScore() {
        return score;
    }

    public Integer getWinsCount() {
        return winsCount;
    }

    public Integer getLosesCount() {
        return losesCount;
    }

    public Player(String nickname) {
        this.nickname = nickname;
    }
}
