package ru.ayupovdamir.models;

import java.time.LocalDateTime;

public class Game {
    private String id;
    private LocalDateTime startGameDateTime;
    private LocalDateTime finishDateTime;
    private Player firstPlayer;
    private Player secondPlayer;
    private Integer shotsCount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDateTime getStartGameDateTime() {
        return startGameDateTime;
    }

    public void setStartGameDateTime(LocalDateTime startGameDateTime) {
        this.startGameDateTime = startGameDateTime;
    }

    public LocalDateTime getFinishDateTime() {
        return finishDateTime;
    }

    public void setFinishDateTime(LocalDateTime finishDateTime) {
        this.finishDateTime = finishDateTime;
    }

    public Player getFirstPlayer() {
        return firstPlayer;
    }

    public void setFirstPlayer(Player firstPlayer) {
        this.firstPlayer = firstPlayer;
    }

    public Player getSecondPlayer() {
        return secondPlayer;
    }

    public void setSecondPlayer(Player secondPlayer) {
        this.secondPlayer = secondPlayer;
    }

    public Integer getShotsCount() {
        return shotsCount;
    }

    public void setShotsCount(Integer shotsCount) {
        this.shotsCount = shotsCount;
    }

    public Game(String id) {
        this.id = id;
    }
}
