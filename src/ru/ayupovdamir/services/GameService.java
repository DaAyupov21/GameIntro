package ru.ayupovdamir.services;

public interface GameService {
    /**
     * Начинает игру. В случае если игрок был в системе
     * то будет использованы его данные если игрока не было в системе
     * до этого, игрок создается заново
     * @param firstPlayer никнейм первого игрока
     * @param secondPlayer никнейм второго игрока
     * @return id игры
     */
    String startGame(String firstPlayer, String secondPlayer);

    void finishGame(String gameId);

    boolean shot(String gameID,String shooterNickname, String targetNickname);
}
