package ru.ayupovdamir.services;

import jdk.jshell.spi.ExecutionControl;
import ru.ayupovdamir.models.Game;
import ru.ayupovdamir.models.Player;
import ru.ayupovdamir.models.Shot;
import ru.ayupovdamir.repositories.GamesRepository;
import ru.ayupovdamir.repositories.PlayersRepository;
import ru.ayupovdamir.repositories.ShotsRepository;

import java.time.LocalDateTime;
import java.util.Random;
import java.util.UUID;

public class GameServiceImpl implements GameService {

    private PlayersRepository playersRepository;
    private GamesRepository gamesRepository;
    private ShotsRepository shotsRepository;
    private Random random;

    public GameServiceImpl(PlayersRepository playersRepository, GamesRepository gamesRepository, ShotsRepository shotsRepository) {
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
        this.shotsRepository = shotsRepository;
        this.random = new Random();
    }

    @Override
    public String startGame(String firstPlayer, String secondPlayer) {

        Player first = getPlayer(firstPlayer);
        Player second = getPlayer(secondPlayer);

        playersRepository.save(first);
        playersRepository.save(second);

        Game game = new Game(UUID.randomUUID().toString());
        game.setFirstPlayer(first);
        game.setSecondPlayer(second);
        game.setShotsCount(0);
        game.setStartGameDateTime(LocalDateTime.now());

        gamesRepository.save(game);
        return game.getId();
    }

    @Override
    public void finishGame(String gameId) {
        Game game = gamesRepository.findGameById(gameId);
        game.setFinishDateTime(LocalDateTime.now());
        try {
            gamesRepository.update(game);
        } catch (ExecutionControl.NotImplementedException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public boolean shot(String gameId, String shooterNickname, String targetNickname) {
        int chance = random.nextInt(10);
        if (chance > 4){
            Player shooter = playersRepository.findPlayerByNickname(shooterNickname);
            Player target = playersRepository.findPlayerByNickname(targetNickname);
            Game game = gamesRepository.findGameById(gameId);
            if (game.getShotsCount() == null){
                game.setShotsCount(0);
            }
            game.setShotsCount(game.getShotsCount() + 1);
            Shot shot = new Shot(game);
            shot.setShooter(shooter);
            shot.setTarget(target);
            shotsRepository.save(shot);
            if (shooter.getScore() == null){
                shooter.setScore(0);
            }
            shooter.setScore(shooter.getScore() + 1);
            return true;
        }
        return false;
    }

    private Player getPlayer(String nicknamePlayer) {
        Player player;
        //если у меня в репозитории был такой игрок, то я просто возьму его, если нет то инициализирую его
        if(playersRepository.containsNickname(nicknamePlayer)){
            player = playersRepository.findPlayerByNickname(nicknamePlayer);
        }
        else {
            player = new Player(nicknamePlayer);
        }
        return player;
    }
}
